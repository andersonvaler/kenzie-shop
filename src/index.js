import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { PersistGate } from "redux-persist/integration/react";

import { Provider } from "react-redux"; //Importar o Provider***
import { store, persistor } from "./store"; // Importar a store criada dentro da pas scr/store/index.js
//Depois passar essa store por prop para o Provider

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate store={store} persistor={persistor} />
      {/*Colocar o App como filho de provider*****/}
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
