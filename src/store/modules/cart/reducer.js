const cartReducer = (state = [], action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      const { product } = action;
      return [...state, product];
    case "REMOVE_FROM_CART":
      const newList = state.filter((product) => product.id !== action.id);
      return newList;
    default:
      return state;
  }
};

export default cartReducer;
