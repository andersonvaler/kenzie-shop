const defaultState = [
  {
    id: 1,
    name: "Suporte para notebook",
    price: 140,
    info:
      "Mesa suporte articulado para Notebook com Mousepad com ventilaçao, travas 360°, ângulo e altura ajustáveis",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_745631-MLB43418189977_092020-O.webp",
  },
  {
    id: 2,
    name: "Fones de ouvido bluetooth",
    price: 135,
    info:
      "Com os Xiaomi AirDots S, você não perde nenhum detalhe e ouve o áudio como ele foi projetado pelos criadores.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_813112-MLA43815118001_102020-O.webp",
  },
  {
    id: 3,
    name: "Teclado e mouse sem fio",
    price: 157,
    info:
      "Este pequeno e inteligente teclado tem todas as teclas padrão — por isso você pode economizar bastante espaço sem perder nenhuma tecla.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_695905-MLB43954170336_102020-O.webp",
  },
  {
    id: 4,
    name: "Webcam com microfone",
    price: 167,
    info:
      "Webcam de ótima qualidade, HD 1080, Cabo USB de bom tamanho, já com microfone integrado, é só plugar e usar, instalação automática.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_833307-MLB44974724770_022021-O.webp",
  },
  {
    id: 5,
    name: "Microfone de mesa profissional",
    price: 124,
    info:
      "O Microfone condensador de alta sensibilidade profissional é um dos mais completos do mercado, com ótimo custo benefício, ideal para uso profissional e amador, exercendo suas gravações com maestria.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_741957-MLB43277416095_082020-O.webp",
  },
  {
    id: 6,
    name: "Iluminador ring light",
    price: 99,
    info:
      "Iluminador Ring Light de 26 centímetros, desenvolvido para maquiadores, fotógrafos, cinegrafistas e etc, que buscam uma ótima maneira de iluminar com perfeição.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_867226-MLB44217781114_122020-O.webp",
  },
  {
    id: 7,
    name: "Teclado mecânico RGB",
    price: 345,
    info:
      "Sua ergonomia, sua base antiderrapante e seu rápido tempo de resposta permitem que seus jogos favoritos se sintam mais próximos do que nunca, na ponta dos dedos.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_771118-MLA44438448894_122020-O.webp",
  },
  {
    id: 8,
    name: "Caixa de som para PC",
    price: 89,
    info:
      "Os alto-falantes são pequenos e altos, não ocupam muito espaço na área de trabalho e possui luzes RGB dinâmicas.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_753275-MLB44394324298_122020-O.webp",
  },
  {
    id: 9,
    name: "Cadeira gamer",
    price: 697,
    info:
      "Cadeira Giratória Gamer possui características ergonomicamente amigáveis que permitem longas sessões de jogos online ou trabalho e evitam o desgaste físico e o cansaço.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_948604-MLB44926444170_022021-O.webp",
  },
  {
    id: 10,
    name: "HD externo 1TB",
    price: 335,
    info:
      "A simplicidade e eficiência de desempenho da linha WD Elements fazem a diferença quando se trata de unidades portáteis. Seu desenho leve e fácil de transportar o torna um dispositivo utilizável em praticamente qualquer lugar.",
    image:
      "https://http2.mlstatic.com/D_NQ_NP_909664-MLA40177545837_122019-O.webp",
  },
];

const productReducer = (state = defaultState, action) => {
  return state;
};

export default productReducer;
