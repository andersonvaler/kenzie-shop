import { useSelector } from "react-redux";
import React from "react";
import Button from "@material-ui/core/Button";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import { makeStyles } from "@material-ui/core/styles";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import CartCard from "../CartCard";
import Badge from "@material-ui/core/Badge";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
}));

export default function Cart() {
  const cart = useSelector((state) => state.cart);

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <div className={classes.root}>
      <div>
        <Button
          ref={anchorRef}
          aria-controls={open ? "menu-list-grow" : undefined}
          aria-haspopup="true"
          onClick={() => handleToggle()}
        >
          <Badge
            badgeContent={cart.length}
            style={{ color: "#fff" }}
            color="primary"
          >
            <ShoppingCartOutlinedIcon
              fontSize="large"
              style={{ color: "#fff" }}
            />
          </Badge>
        </Button>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom",
              }}
            >
              <Paper id="papper-p">
                {cart.length === 0 && (
                  <p>
                    <br />
                    Carrinho vazio!
                  </p>
                )}
                {cart &&
                  cart.map((product, index) => (
                    <CartCard product={product} key={index} />
                  ))}
                {cart.length !== 0 && (
                  <div id="total">
                    <label>
                      Total: R${" "}
                      {cart
                        .reduce((acc, product) => acc + product.price, 0)
                        .toFixed(2)}
                    </label>
                  </div>
                )}
                <MenuList autoFocusItem={open} id="menu-list-grow">
                  <MenuItem onClick={handleClose}>Fechar</MenuItem>
                </MenuList>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    </div>
  );
}
