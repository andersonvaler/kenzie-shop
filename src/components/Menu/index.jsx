import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import "./style.css";
import Cart from "../Cart";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function Menu() {
  const classes = useStyles();

  return (
    <div className={classes.root} id="menu">
      <AppBar position="static">
        <Toolbar>
          <Cart></Cart>
          <Typography variant="h6" className={classes.title} id="title">
            Kenzie Shop
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
