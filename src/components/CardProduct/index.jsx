import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../store/modules/cart/actions";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import "./style.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 300,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const CardProduct = ({ product }) => {
  const cart = useSelector((state) => state.cart);
  const { name, price, info, image } = product;

  const classes = useStyles();

  const dispatch = useDispatch();

  const addProductToCart = (product) => {
    if (!cart.includes(product)) {
      dispatch(addToCart(product));
    }
  };

  return (
    <Card className="card">
      <CardContent>
        <Typography variant="h5" component="h2">
          {name}
        </Typography>
        <div className="img">
          <figure style={{ backgroundImage: `url(${image})` }} />
        </div>
        <hr />
        <Typography className={classes.pos} id="price">
          R$ {price.toFixed(2)}
        </Typography>
        <hr />
        <Typography variant="body2" component="p" id="info">
          {info}
        </Typography>
      </CardContent>

      <CardActions id="button" onClick={() => addProductToCart(product)}>
        <p>Adicionar ao carrinho</p>
      </CardActions>
    </Card>
  );
};

export default CardProduct;
