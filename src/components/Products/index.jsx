import { useSelector } from "react-redux";
import CardProduct from "../CardProduct";
import "./style.css";
const Products = () => {
  const products = useSelector((state) => state.products);
  return (
    <>
      <div className="products">
        {products.map((product, index) => {
          return (
            <section className="product" key={index}>
              <CardProduct product={product}></CardProduct>
            </section>
          );
        })}
      </div>
    </>
  );
};

export default Products;
