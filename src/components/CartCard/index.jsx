import React from "react";
import { useDispatch } from "react-redux";
import { removeFromCart } from "../../store/modules/cart/actions";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import "./style.css";

const CartCard = ({ product }) => {
  const { name, price, image, id } = product;
  const dispatch = useDispatch();
  return (
    <>
      <div className="card-cart">
        <div className="card-img">
          <figure style={{ backgroundImage: `url(${image})` }} />
        </div>

        <div className="product-info">
          <h3 className="product-name">{name}</h3>
        </div>

        <div className="product-price">
          <div className="cart-price">R$ {price.toFixed(2)}</div>
        </div>
        <HighlightOffIcon
          fontSize="large"
          style={{ color: "red" }}
          className="cart-button"
          onClick={() => dispatch(removeFromCart(id))}
        />
      </div>
    </>
  );
};

export default CartCard;
