import Menu from "./components/Menu";
import Products from "./components/Products";

const App = () => {
  return (
    <div>
      <Menu />
      <Products></Products>
    </div>
  );
};

export default App;
